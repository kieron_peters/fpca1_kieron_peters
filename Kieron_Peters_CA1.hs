{-

    This File contains multiple functions that are mathematical oriented
    loading this file into winGHCi will then allow you to use these function
    names defined below to carry out functions according to their relavant
    names. There are 18 functions for haskell defined below with their type
    definition followed by their operations, comments will be added for
    each function to describe clearly what they do.
    Author of File: Kieron Peters
    Created: 27/02/2016

-}

{- 

    this function takes in two numbers and outputs one number of typeclass Num
    as using this typeclass allows integers and floats to be combined together
    no guards or base cases needed here as 0 can be added to another 0 and 
    doubled to get 0 and is not recursive
    is written by add_and_double a b
    
    This function is used by writing: add_and_double 2 3
    
-}

import Data.CaseInsensitive (foldCase)

add_and_double :: Num a => a -> a -> a
add_and_double a b = (a+b)*2

{-
    useful link helping me define function from reddit on Haskell at: 
    https://www.reddit.com/r/haskell/comments/1a3jv4/how_do_i_define_infix_functions_in_haskell/
    
    this function defines infix operator +* and does not use backtick notation as symbols can't use
    backticks, then later I go on to define add_and_double' as this operator and this can be used by writing
    a `add_and_double'` b or by: 4 `add_and_double'` 6
    
    
    
-}

(+*) :: Num a => a -> a -> a
(+*) a b = (a+b)*2
add_and_double'  = (+*)

{-
    attempted to use below for the square root function
    x = (-b + (sqrt((b^2)- 4*(a*c))))/(2*a)
    this did not calculate both however so I had to separate into
    different variables
    This link helped me to define x's two possible values
    https://en.wikipedia.org/wiki/Plus-minus_sign
    Useful if condition came from this page on stackoverflow:
    http://stackoverflow.com/questions/7794692/haskell-quadratic-equation-root
    
    this function takes in 3 double numbers and returns a tuple of the highest
    and lowest answers for this equation, this takes in the numbers, creates
    variables to hold the discriminant from the equation and returns the
    result to the user by calculating them in another variable for each tuple
    
    This function is used by writing: solve_quadratic_equation 5 12 5
    
-}



solve_quadratic_equation :: Double -> Double -> Double -> (Double,Double)
solve_quadratic_equation a b c =
    if discriminant < 0 || isNaN lower || isNaN upper then error "negative result" else (lower,upper)
            where upper = ( - b + discriminant) / (2 * a)
                  lower = ( - b - discriminant) / (2 * a)
                  discriminant = sqrt(b * b - 4 * a * c)

{-
    
    This function takes in one value, I implemented one before and took
    in two numbers, and returned that number n times, but was incorrect
    the correct implementataion is below where you enter a number
    if the number is 0, return an empty list, then a guard is used
    where an error is shown if n is less than 0 entered in, otherwise
    the function take n from an infinite list is used.
    
    This function is used by writing: first_n 12
    
-}

first_n :: Int -> [Int]
first_n 0 = []
first_n n
        |n < 0 = error "less than 0"
        |otherwise = take n [1..]

{-
    
    This site helped in syntax for defining where clause for a local helper function:
    http://stackoverflow.com/questions/418120/whats-your-naming-convention-for-helper-functions
    I could not work out how to make the first function generate the list to pass into the
    helper function, if I made it recursive itself, would eliminate the helper functions recursiveness
    and using replicate does not work as list entry as type is Integer not Int for first arguement
    
    This function is the same as first_n but uses a helper function that is recursive
    and validates the input is greater than 0 entered in and takes the infinate list into it
    This then returns either empty list if n == 0 or error is less than o and recursively
    draws out the numbers one at a time and returns this list to first_n_integers
    
    This function is used by writing: first_n_integers 7
    
-}

first_n_integers :: Integer -> [Integer]
first_n_integers n = take_integer n [1..]
        where take_integer :: Integer -> [Integer] -> [Integer]
              take_integer n (x:xs)
                  |n < 0 = error "Negative Value"
                  |n == 0 = []
                  |otherwise = x : take_integer (n - 1) xs

{-
    
    This site helped me understand factorials of odd and even for
    double factorial definitions:
    http://whatis.techtarget.com/definition/double-factorial
    
    This function takes in an Integer type and shows you a list back
	of each calculation of all the factorial results for each value in
	the list down to 0 which equates to 1
    
    This function is used by writing: double_factorial 20
    
-}

double_factorial :: Integer -> [Integer]
double_factorial n
        |n == 0 = []
        |otherwise = factorial (n - 1) : double_factorial (n - 1)
                where factorial :: Integer -> Integer
                      factorial n
                          |n == 0 = 1
                          |n < 0 = error "Negative Number Entered"
                          |otherwise = n * factorial (n - 1)

{-
    
    This site helped me define how to use zipWith function
    http://zvon.org/other/haskell/Outputprelude/zipWith_f.html
	Very useful link to factorails:
	http://scienceblogs.com/goodmath/2006/11/28/simple-functions-in-haskell-1/
    
    This function takes in the value from the user to use like the
	previous function but just uses an infinate list along with a list
	from 0 to the value they entered in, and the infinate list is in
	the opposite direction so using zipWith by multiplying you get 0 x 0,
	1 x 1 and so on, the definition of factorial, I discovered how to do 
	this thanks to the reference from above on defining factorials in haskell
    
    This function is used by writing: factorials 6
    
-}

factorials :: Integer -> [Integer]
factorials n
        |n < 0 = []
        |n == 0 = []
        |otherwise =  1 : zipWith (*) [1..] (factorials (n - 1))

{-
    
    This site helped me indetify what a prime number definition is:
    http://www.mathsisfun.com/definitions/prime-number.html
    
    After some long time thinking, I had to define a recursive function
    that uses a string of boolean checking and some Integer values
    first we guard to see if n is 2 and return true for this and then
    we will see if the number divides evenly into itself and then compare with
    the second function that calls itself and checks each n down to 2 where we
    know it is true and so only if each call is true where it does not divide
    evenly into all the following numbers except one, we can return true of false
    for it a number is really prime or not
    
    This function is used by writing: isPrime 42
    
-}

isPrime :: Integer -> Bool
isPrime n
        |n == 2 = True
        |n < 0 = error "Negative Number Entered"
        |otherwise = n `mod` n == 0 && isPrime' n (n - 1)

isPrime'::Integer -> Integer -> Bool
isPrime' a n
        |n == 2 = n `mod` n == 0
        |otherwise = a `mod` n /= 0 && isPrime' a (n - 1)

{-
    
    This function is inspired from my previous success from defining a new function to help
    with the primes and I decided to try it again and have trouble with the syntax to build
    a local helper function so it is defined separately, first I allow the user to enter a
    simple number of how many primes they want with primes and then this calls primeCount with
    two values, the number they want and the value 2 for the first prime number, then the new
    function has a guard to return an empty list otherwise check in if statement using if prime
    is true then we know it is prime then we concatenate this n with the call to itself with n up one
    to the next number and a is down one to show we need one less number to this list otherwise we keep
    as it is because we still need to add a number to the list and we increase n to the next number to
    compare to if it is prime or not and is a recursive function we have a base case of when a hits 0 to
    return an empty list value to allow the function to end from an infinite call
    
    This function is used by writing: primes 6
    
-}

primeCount :: Integer -> Integer -> [Integer]
primeCount a n
        |a == 0 = []
        |a < 0 = error "Negative Number Entered"
        |otherwise = if isPrime n == True then n : primeCount (a - 1) (n + 1) else primeCount a (n + 1) 


primes :: Integer -> [Integer]
primes n = primeCount n 2

{-
    
    This function I remembered from class to try and split a list's head and then
    add it to a recursive call to the function with the rest of the list and the base
    case is an empty list that adds 0 to the count
    
    This function is used by writing: sumIntegerList [1,2,3,4]
    
-}

sumIntegerList :: [Integer] -> Integer
sumIntegerList [] = 0;
sumIntegerList (x:xs) = x + sumIntegerList xs

{-
    
    These sites helped me understand how to define and use foldl:
    https://en.wikibooks.org/wiki/Haskell/Lists_III
    http://zvon.org/other/haskell/Outputprelude/foldl_f.html
    
    This function takes in a list of integers and returns out the sum
    the same as the last function but uses the foldl function with 0
    and the function + to add each element of the list together also
    negative numbers work to be added together correctly, no base case
    as the function foldl has its own base cases already defined
    
    This function is Used by Writing : sumIntegerListFoldl [1,2,3,4]
    
-}

sumIntegerListFoldl :: [Integer] -> Integer
sumIntegerListFoldl xs = foldl (+) 0 xs

{-
   
    This function calls the same to above of foldr but with * as the
    function to pass in along with one to multiply each number by before
    adding it to the list, this also does not need a base case as it
    is also calling a function called foldr that has base cases already
    defined for it, the input of negative numbers is okay for multiplying
    as the number can flip between negative and positive answers as you would
    expect a normal multiplication to be
    
    This function is used by writing: multiplyIntegerListFoldr [1,2,3,4,5,6]
    
-}

multiplyIntegerListFoldr :: [Integer] -> Integer
multiplyIntegerListFoldr [] = 0;
multiplyIntegerListFoldr xs = foldr (*) 1 xs

{-
    
    This site was useful on StackOverflow about imports but unsure if we had to
    use these and caused parse errors in other places when imported at the top of
    the page:
    http://stackoverflow.com/questions/24533874/case-sensitive-insensitive-comparisons-for-data-text
    I then discovered this page and looking through found foldCase as a possible match to my needs:
	https://hackage.haskell.org/package/case-insensitive-1.2.0.5/docs/Data-CaseInsensitive.html
	
    This function does return the correct response for the String and value entered by passing the string s
	through the Data.CaseInsenstive function foldCase , this function takes the input string and returns the string
	all in lower case format, so I then have the string to compare it to all written in lowercase, regardless of how
	many capitals are in the string, as long as the words are written correctly, the statement will return true
    
    This function is used by writing: guess "I love fuNctionaL ProgramMing" 4
-}

guess :: String -> Int -> String
guess s n
        |foldCase s == "i love functional programming" && n < 5  = "You have won"
        |foldCase s /= "i love functional programming" && n < 5 = "Guess again"
        |otherwise = "you have lost"

{-
    
    This site helped me define what a dot product was:
    https://www.mathsisfun.com/algebra/vectors-dot-product.html
    
    This function was written using above link and remembered how to
    use list comprehension to get out the list, I then used sum function
    on the list comprehension so that you can sum all the outputs the list
    will generate from the tuples of x's and y's passed into it
    
    This function is used by writing: dotProduct [(2,4),(4,8),(8,16)]
    
-}

dotProduct :: (Num a) => [(a,a)] -> a
dotProduct xs = sum[a * b| (a,b)<- xs]

{-
    
    This function was easy for me to implement using the modulus operator
    function and if the number passed in does not evenly divide into 2 then
    the number is not even and false is returned otherwise true is returned
    when the function is ran this has been tested with negative numbers and
	0 I consider to be even and -1 to be odd and -2 to be even and so on
    
    This function is used by writing: is_even 2
    
-}

is_even :: Int -> Bool
is_even n = if n `mod` 2 == 0 then True else False

{-
    
    Very good site link on StackOverflow to help define how to get each
    vowel out of a string and remove it, then return that altered string
    back out to the user:
    http://stackoverflow.com/questions/32286914/remove-all-vowels-of-a-string-with-recursive-function
    
    This function uses recursion to call down to the base case of an empty list as a String is
    really just a list of Chars and so this list can get an x that can be check that is does
    not have a element of aeoi or u in it and then concatenates it with recursive call otherwise
    just calls the recursive call on the rest of the String of Chars this has been tested and
    does not allow upper or lower case letters to be entered that are vowels, I realised that my base
    case was not working while finishing up censor so I had the idea that if the list was blank, I needed
    to have one more if condition that checked one more time that x was not an element and then put the x
    into the list otherwise leave it blank, condition checks for empty string before running at all
    	
    This function is used by writing: unixName "This is my house"
    
-}

unixName :: String -> String
unixName [] = error "Please enter something to check"
unixName (x:xs)
        |xs == [] = if not(x `elem` "aeiouAEIOU") then [x] else []
        |not(x `elem` "aeiouAEIOU") = x : unixName xs
        |otherwise = unixName xs


{-
    
    I had no luck finding out how to do this online but I did use another defined function
    below to get a working model, I had got it to compare a value but only if they were both
    in the same position so I thought about checking each element in the first list with every
    element in the second list and I needed to use a second function called checkList to do it
    first you take the (x:xs) and ys into intersection and then you call listCheck on that x and
    ys then concatenate it with intersection again as recursive on the rest of xs and all ys again
    listCheck then returns a single list of that x if it is matching y:ys the y matching otherwise
    calls itelf with the x and the rest of ys recursively, the base cases are covering emply lists
    passed in and returns the list blank added on to whatever is matching, this function is tested
    with numbers and negative and positive do not match the same and Chars do not match if they are
    not both matching upper or lower case
    
    This function is used by writing: intersection [2,6,7] [1,2,3,7]
    
-}

listCheck :: (Eq a) => a -> [a] -> [a]
listCheck x [] = []
listCheck x (y:ys)
        |x == y = [x]
        |otherwise = listCheck x ys


intersection :: (Eq a) => [a] -> [a] -> [a]
intersection _ [] = []
intersection [] _ = []

intersection (x:xs) ys = listCheck x ys ++ intersection xs ys

{-
    
    This was worked using the same approach as unixName but the only difference is
    that the if condition and guard were changed for otherwise you added a Char 'x'
    onto the list before calling the function again recursively as well as changing
    the base case condition to put a Char 'x' into the list instead of leaving it as
    blank as this last character could be either an aeiouAEIOU value or not as in unixName
	this function also checks that an empty string has not been entered
    
    This function is used by writing: censor "this is my house"
    
-}



censor :: String -> String
censor [] = error "Please enter something to censor"
censor (x:xs)
        |xs == [] = if not(x `elem` "aeiouAEIOU") then [x] else ['x']
        |not(x `elem` "aeiouAEIOU") = x : censor xs
        |otherwise = 'x' : censor xs
